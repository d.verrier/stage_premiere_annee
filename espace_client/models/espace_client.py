# Création de table et de leur champ dans PostgreSQL

# -*- coding: utf-8 -*-

from odoo import fields, models

# Création d'une table 'espace.client.catalogue avec différents champs (name, description, type, prix) et ceux créés automatiquement par Odoo
class EspaceClientCatalogue(models.Model):

    #nom de l'objet métier, en notation par points (dans l'espace de noms du module)
    _name = 'espace.client.catalogue'
    _description = 'Catalogue de l\'espace client'
    #Champ de commande lors de la recherche sans ordre spécifié (par défaut: 'id' )
    #Type: str
    _order = 'name asc'

    name = fields.Char('Nom de l\'article', required=True)
    description = fields.Text('Description')
    type = fields.Char('Type d\'article', required=True)
    prix = fields.Float('Prix', required=True)

# Création d'une table 'espace.client.catalogue avec différents champs (date, espace_client_catalogue_id, partner_id, company_id) et ceux créés automatiquement par Odoo
class EspaceClientCommande(models.Model):

    #nom de l'objet métier, en notation par points (dans l'espace de noms du module)
    _name = 'espace.client.commande'
    _description = 'Commande de l\'espace client'
    #Champ de commande lors de la recherche sans ordre spécifié (par défaut: 'id' )
    #Type: str
    _order = 'date desc'

    date = fields.Date(help='Date à laquelle l\'achat de l\'article a été effectué')
    #Clé étrangère venant de la table espace.client.catalogue
    espace_client_catalogue_id = fields.Many2one('espace.client.catalogue', 'Article', help='Article acheté')
    #Clé étrangère venant de la table res.partner
    partner_id = fields.Many2one('res.partner', 'Clients', help='Informations du client')
    #Clé étrangère venant de la table res.company
    company_id = fields.Many2one('res.company', 'Entreprise', help='Informations de l\'entreprise')
