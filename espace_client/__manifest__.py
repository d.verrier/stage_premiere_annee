# Ce fichier manifeste sert à déclarer ce package python en tant que module Odoo et à spécifier les métadonnées de ce module.
# Ce fichier appelé __manifest__.py contient un seul dictionnaire Python, où chaque clé spécifie la métadonnée de ce module.

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#Les champs de manifeste
{
    #name ( str obligatoire)
    #Le nom du module lisible par l'homme
    'name' : 'Espace Client',
    #version ( str )
    #La version de ce module, devrait suivre les règles de versioning sémantiques
    'version' : '1.0',
    'summary' : 'Articles, Factures, Informations',
    #description ( str )
    #description étendue de ce module, dans reStructuredText
    'description' :  """
Articles, Factures, Informations
==================================
Avec ce module, Odoo vous aide à gérer tout votre espace client

Caractéristiques principales
-------------
* Voir un catalogue d'articles
* Voir toutes les factures d'articles achetés
* Voir ses informations personnelles
* Voir les informations de l'entreprise
""",
    #depends ( list(str))
    #Les modules Odoo qui doivent être chargés avant celui-ci, soit parce que ce module utilise les fonctionnalités qu'ils créent, soit parce qu'il modifie les ressources qu'ils définissent.
    #Lorsque ce module est installé, toutes ses dépendances sont installées avant. De même, les dépendances sont chargées avant le chargement d'un module.
    'depends' : [
        'base',
        ],
    #data ( list(str))
    #Liste des fichiers de données qui doivent toujours être installés ou mis à jour avec ce module.
    #Une liste des chemins du répertoire racine de ce module
    'data' : [
        'security/ir.model.access.csv',
        'views/espace_client_vues_formulaire.xml',
        'views/espace_client_vues_liste.xml',
        'views/espace_client_vues_recherche.xml',
        'views/espace_client_vues_kanban.xml',
        'views/espace_client_menu.xml',
        'data/espace_client.xml',
        ],
    #installable ( bool Par défaut: False )
    #Indique que l'utilisateur est capable d'installer ce module à partir de l'interface utilisateur Web.
    'installable' : True,
    #application ( bool, Par défaut: False )
    #Indique que le module est considéré comme une application à part entière ( True ) ou qu'il s'agit d'un module technique ( False ) fournissant des fonctionnalités supplémentaires à un module d'application existant.
    'application' : True,
}
